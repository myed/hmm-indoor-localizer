% -------------------------------------------------------------------
%% Plot map with posterior
% -------------------------------------------------------------------
hold off
sizeGridX = nRows+1;
sizeGridY = nCols+1;
for i=1:sizeGridY
    plot([1 sizeGridX]-0.5,[i i]-0.5,'-','color',[.6,.6,.6]);hold on;
end
for i=1:sizeGridX
    plot([i i]-0.5,[1 sizeGridY]-0.5,'-','color',[.6,.6,.6]);hold on;
end
axis off

for i = 1:nRows
    for j=1:nCols
        isObstacle = false;
        for o=1:sizeObstacles
            if i == obstacles(o,1) && j == obstacles(o,2)
                isObstacle = true; break;
            end
        end
        
        if isObstacle
            text(i-.28,j,'X','color',[.6 .6 .6], ...
                'fontsize', 20, 'BackgroundColor',[.6 .6 .6])                
        else
        	ms = posterior(i,j)*20 +.00000001;
        	plot(i,j,'marker', 'o', 'markerfacecolor', 'black', 'markersize', ms)
        end
    end
end

%hFig = figure(1);
%set(hFig, 'Position', [100 100 700 400])